# Micro Challenge IV MDEF Personal Reflection

## **Initial Questions**
#### 1. Is Vertical Farming the Future of Food and Agriculture?
#### 2. Could Duckweed Play a Role in this Future?
#### 3. What Are All the Potential Real-World Applications of Duckweed Besides Food?
#### 4. How Many Decades Will it Take for the Majority of Humanity to Shift to a Plant-Based Diet?
#### 5. Is This Even Realistically Achievable?
#### 6. How Much of Our Food Can We Grow Inside or Close to Our Urban Areas & Cities? 

## **Facts: An Objective Account of What Happened**

#### The central target of this challenge since the beginning was to prepare a final prototype to present at the MDEFest. From my side, I wanted to create some sort of bioreactor that could grow a "futuristic" food source, preferably microalgae (spirulina,chlorella,...) or duckweed. Jean-Luc's idea was the creation of this concept called _Proof of Growth_, which would be a bit similar to ECO coin (https://www.ecocoin.com/), but, in this case, the sustainable action backing this cryptocurrency would be growing food correctly. For example, a local urban farmer growing lettuce on an indoor farm would be rewarded using this system by growing his _produce_ with the right conditions in terms of heat, light, water, pH, EC,... The more precise/accurate when controlling & regulating these factors, the bigger his digital wallet would become. However, and unfortunately too, the project wouldn't move towards this direction. We would set up all the sensors and actuators, using all of the accumulated knowledge of the past few weeks (input devices, output devices, interface and application programming and wildcard week). We ended up with a very neat and sophisticated final prototype. The final step was ordering the duckweed online (we first went to a gardening shop in Cabrera de Mar, but to no avail...). It still took a couple of weeks this final step, but, since we prepared ourselves properly beforehand (did everything with antecipation), it was totally chill...

## **Feelings: The Emotional Reactions to the Situation**

#### This last challenge almost approached perfection, literally speaking. I felt so overjoyed at the end!! We had learned from the failures, mistakes and setbacks of the previous challenges and we were 100% prepared to succeed. We did everything with time and granularity, so it couldn't have turned out better honestly. We didn't leave any stone unturned... 

## **Findings: The Concrete Learning That You Can Take Away From the Situation**

#### Patience, consistency, persistence/resilience and belief always wins in the end!! Besides that, I finally comprehend the basic foundations of all the Fab Academy weekly content and classes. But that came mostly from a lot of study and many hours of hands-on work, plus a lot of help and guidance from Santi, Oscar, Josep and Edu. Most importantly, unlike I initially felt in September, I now feel 100% confident that mastering all these subjects & topics is totally possible!! I can now also confidently call myself a multipotentialite: entrepreneur, designer and maker all in one...

## **Future: Structuring Your Learning Such that You Can Use it in the Future**

#### The most important thing for me when I finish this Master is to continue studying all the content and material, find a Fab Lab in Lisbon to continue practicing & developing my skills and never stop making things!! Thank you!! 
